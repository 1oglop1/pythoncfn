import typing
import attr


@attr.s
class VPC:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-vpc.html

    """
    CidrBlock = attr.ib(type=str)
    EnableDnsHostnames = attr.ib(type=bool, kw_only=True, default=None)
    EnableDnsSupport = attr.ib(type=bool, kw_only=True, default=None)
    InstanceTenancy = attr.ib(type=str, kw_only=True, default=None)
    Tags = attr.ib(type=typing.List['Tag'], kw_only=True, default=None)
    attributes = attr.ib(init=False, default=[{'name': 'CidrBlock', 'type': 'str'}, {'name': 'CidrBlockAssociations', 'type': 'List'}, {'name': 'DefaultNetworkAcl', 'type': 'str'}, {'name': 'DefaultSecurityGroup', 'type': 'str'}, {'name': 'Ipv6CidrBlocks', 'type': 'List'}])


