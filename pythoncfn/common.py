import typing
import attr


@attr.s
class Tag:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-resource-tags.html

    """
    Key = attr.ib(type=str)
    Value = attr.ib(type=str)
    attributes = attr.ib(init=False, default='')


