from typing import Optional, List

import attr

from ApiGateway import ApiKey


@attr.s
class AttrsClass:
    """
    Hello

    Parameters
    ----------
    variable: Optional[List[str]]
        ASDF

    :type arg1: type description
    """
    variable = attr.ib(type=Optional[List[str]], kw_only=True, default=['344'])
    # var2: Optional[str] = attr.ib(default=None)



class NormalClass:
    def __init__(self, variable: Optional[List[str]] = None):
        self.variable = variable


ac = AttrsClass()
nc = NormalClass()

print(help(AttrsClass))

print(AttrsClass.__doc__)
print(help(NormalClass))

