import typing
import attr


@attr.s
class ActivatedRule:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-waf-webacl-rules.html

    """
    Action = attr.ib(type=WafAction, kw_only=True, default=None)
    Priority = attr.ib(type=int)
    RuleId = attr.ib(type=str)
    attributes = attr.ib(init=False, default='')


@attr.s
class WafAction:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-waf-webacl-action.html

    """
    Type = attr.ib(type=str)
    attributes = attr.ib(init=False, default='')


@attr.s
class WebACL:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-waf-webacl.html

    """
    DefaultAction = attr.ib(type=WafAction)
    MetricName = attr.ib(type=str)
    Name = attr.ib(type=str)
    Rules = attr.ib(type=typing.List['ActivatedRule'], kw_only=True, default=None)
    attributes = attr.ib(init=False, default='')


