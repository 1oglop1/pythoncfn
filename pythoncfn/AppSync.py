import typing
import attr


@attr.s
class ApiKey:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-appsync-apikey.html

    """
    Description = attr.ib(type=str, kw_only=True, default=None)
    Expires = attr.ib(type=float, kw_only=True, default=None)
    ApiId = attr.ib(type=str)
    attributes = attr.ib(init=False, default=[{'name': 'ApiKey', 'type': 'str'}, {'name': 'Arn', 'type': 'str'}])


