import typing
import attr


@attr.s
class StageKey:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-apigateway-apikey-stagekey.html

    """
    RestApiId = attr.ib(type=str, kw_only=True, default=None)
    StageName = attr.ib(type=str, kw_only=True, default=None)
    attributes = attr.ib(init=False, default='')


@attr.s
class ApiKey:
    """
    Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html

    """
    CustomerId = attr.ib(type=str, kw_only=True, default=None)
    Description = attr.ib(type=str, kw_only=True, default=None)
    Enabled = attr.ib(type=bool, kw_only=True, default=None)
    GenerateDistinctId = attr.ib(type=bool, kw_only=True, default=None)
    Name = attr.ib(type=str, kw_only=True, default=None)
    StageKeys = attr.ib(type=typing.List['StageKey'], kw_only=True, default=None)
    attributes = attr.ib(init=False, default='')


