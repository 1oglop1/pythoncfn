# from mock import mock_open, patch
import json

schema_json = {
    'ResourceSpecificationVersion': "0.0.1",
    "PropertyTypes": {
        "AWS::ApiGateway::ApiKey.StageKey": {
            "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-apigateway-apikey-stagekey.html",
            "Properties": {
                "RestApiId": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-apigateway-apikey-stagekey.html#cfn-apigateway-apikey-stagekey-restapiid",
                    "PrimitiveType": "String",
                    "Required": False,
                    "UpdateType": "Mutable"
                },
                "StageName": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-apigateway-apikey-stagekey.html#cfn-apigateway-apikey-stagekey-stagename",
                    "PrimitiveType": "String",
                    "Required": False,
                    "UpdateType": "Mutable"
                }
            }
        },
    },
    "ResourceTypes": {
        "AWS::ApiGateway::ApiKey": {
            "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html",
            "Properties": {
                "CustomerId": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-customerid",
                    "PrimitiveType": "String",
                    "Required": False,
                    "UpdateType": "Mutable"
                },
                "Description": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-description",
                    "PrimitiveType": "String",
                    "Required": False,
                    "UpdateType": "Mutable"
                },
                "Enabled": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-enabled",
                    "PrimitiveType": "Boolean",
                    "Required": False,
                    "UpdateType": "Mutable"
                },
                "GenerateDistinctId": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-generatedistinctid",
                    "PrimitiveType": "Boolean",
                    "Required": False,
                    "UpdateType": "Immutable"
                },
                "Name": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-name",
                    "PrimitiveType": "String",
                    "Required": False,
                    "UpdateType": "Immutable"
                },
                "StageKeys": {
                    "Documentation": "http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-apikey.html#cfn-apigateway-apikey-stagekeys",
                    "DuplicatesAllowed": False,
                    "ItemType": "StageKey",
                    "Required": False,
                    "Type": "List",
                    "UpdateType": "Mutable"
                }
            }
        },
    }
}
