from helpers import parse_aws_name, concat_aws_name


def test_split_aws_name_resource(aws_name_resource, aws_name_parsed_resource):

    assert aws_name_parsed_resource == parse_aws_name(aws_name_resource)


def test_split_aws_name_property(aws_name_property, aws_name_parsed_property):

    return aws_name_parsed_property == parse_aws_name(aws_name_property)


def test_concat_aws_name_property():
    pass

def test_concat_aws_name_resource():
    pass