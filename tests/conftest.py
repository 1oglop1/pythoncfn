import pytest


@pytest.fixture(scope='session')
def aws_name_property():
    return "AWS::ApiGateway::ApiKey.StageKey"


@pytest.fixture(scope='session')
def aws_name_resource():
    return "AWS::ApiGateway::ApiKey"


@pytest.fixture(scope='session')
def aws_name_parsed_resource():
    parsed_resource = {
        'aws': 'AWS',
        'module': 'ApiGateway',
        'class': 'ApiKey',
        'property': None
    }

    return parsed_resource


@pytest.fixture(scope='session')
def aws_name_parsed_property():
    parsed_property = {
        'aws': 'AWS',
        'module': 'ApiGateway',
        'class': 'ApiKey',
        'property': 'StageKey'
    }

    return parsed_property
